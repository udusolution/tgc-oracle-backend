import { IRouteLogger } from "../../src/Common/Interfaces/LoggerInterfaces";
import { Admin } from "../../src/Models/Admin";
import { User } from "../../src/Models/User";

declare global{
    namespace Express {
        interface Request {
            body: any,
            baseUrl: string,
            path: string,
            reqId: string,
            log: IRouteLogger,
            fullPath: string,
            user?: User,
            admin?: Admin
        }
    }
}

export {}
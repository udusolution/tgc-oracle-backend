import moment, { Moment } from "moment";
import { IService, IServiceCategory } from "../Interfaces/DataInterfaces";
import { Service } from "./Service";

export class ServiceCategory implements IServiceCategory {

    // Properties
    id: number;
    title: string;
    description: string;
    createdOn: string | Date | Moment;
    updatedOn: string | Date | Moment;
    is_deleted: boolean;
    services?: Service[] | undefined;

    // Constructor
    constructor(props: IServiceCategory, services?: Service[]) {
        this.id = props.id;
        this.title = props.title;
        this.description = props.description || '';
        this.createdOn = props.createdOn;
        this.updatedOn = props.updatedOn;
        this.is_deleted = props.is_deleted;
        if(services) this.services = services;
    }

    // Methods
    createdOnMoment(): Moment {
        return moment(this.createdOn);
    }
    updatedOnMoment(): Moment {
        return moment(this.updatedOn);
    }
    
}
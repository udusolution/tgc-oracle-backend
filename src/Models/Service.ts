import moment, { Moment } from "moment";
import { IService, IServiceCategory } from "../Interfaces/DataInterfaces";

export class Service implements IService {

    // Properties
    id: number;
    service_category_id: number;
    title: string;
    description?: string | undefined;
    mode: string;
    provider?: string | undefined;
    rate_per_1000?: number | undefined;
    min_order?: number | undefined;
    max_order?: number | undefined;
    increment?: number | undefined;
    overflow?: number | undefined;
    createdOn: string | Date | Moment;
    updatedOn: string | Date | Moment;
    is_deleted: boolean;
    service_category?: IServiceCategory | undefined;

    // Constructor
    constructor(props: IService, serviceCategory?: IServiceCategory) {
        this.id = props.id;
        this.service_category_id = props.service_category_id;
        this.title = props.title;
        this.description = props.description || '';
        this.mode = props.mode;
        if(props.provider != undefined) this.provider = props.provider;
        if(props.rate_per_1000 != undefined) this.rate_per_1000 = props.rate_per_1000;
        if(props.min_order != undefined) this.min_order = props.min_order;
        if(props.max_order != undefined) this.max_order = props.max_order;
        if(props.increment != undefined) this.increment = props.increment;
        if(props.overflow != undefined) this.overflow = props.overflow;
        this.createdOn = props.createdOn;
        this.updatedOn = props.updatedOn;
        this.is_deleted = props.is_deleted;
        if(serviceCategory) this.service_category = serviceCategory;
    }

    // Methods
    createdOnMoment(): Moment {
        return moment(this.createdOn);
    }
    updatedOnMoment(): Moment {
        return moment(this.updatedOn);
    }
    
}
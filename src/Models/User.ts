import moment, { Moment } from "moment";
import { IUser } from "../Common/Auth/Interfaces";


export class User implements IUser {

    // Properties
    id: number;
    username: string;
    email: string;
    avatar?: string | undefined;
    password?: string | undefined;
    privilege?: string | undefined;
    is_banned: boolean;
    is_deleted: boolean;
    createdOn: string | Date | moment.Moment;
    updatedOn: string | Date | moment.Moment;

    // Constructor
    constructor(props: IUser) {
        this.id = props.id;
        this.email = props.email;
        this.username = props.username;
        if(props.avatar) this.avatar = props.avatar;
        if(props.password) this.password = props.password;
        if(props.privilege) this.privilege = props.privilege;
        this.is_banned = !!props.is_banned;
        this.is_deleted = !!props.is_deleted;
        this.createdOn = props.createdOn;
        this.updatedOn = props.updatedOn;
    }
    
    
    // Methods
    createdOnMoment(): Moment {
        return moment(this.createdOn);
    }
    updatedOnMoment(): Moment {
        return moment(this.updatedOn);
    }

}
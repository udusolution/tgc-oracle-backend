import moment, { Moment } from "moment";
import { IAdmin, Role } from "../Common/Auth/Interfaces";


export class Admin implements IAdmin {

    // Properties
    id: number;
    email: string;
    role: Role;
    username: string;
    avatar?: string | undefined;
    password?: string | undefined;
    is_deleted: boolean;
    createdOn: string | Date | Moment;
    updatedOn: string | Date | Moment;

    // Constructor
    constructor(props: IAdmin) {
        this.id = props.id;
        this.email = props.email;
        this.role = props.role;
        this.username = props.username;
        if(props.avatar) this.avatar = props.avatar;
        if(props.password) this.password = props.password;
        this.is_deleted = !!props.is_deleted;
        this.createdOn = props.createdOn;
        this.updatedOn = props.updatedOn;
    }
    
    
    // Methods
    createdOnMoment(): Moment {
        return moment(this.createdOn);
    }
    updatedOnMoment(): Moment {
        return moment(this.updatedOn);
    }

}
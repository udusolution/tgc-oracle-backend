/**
 * Bring in Environment Variables
 */
import * as dotenv from 'dotenv';
dotenv.config({ path: `${__dirname}/.env` });


/**
 * Imports
 */
import express from 'express';
import cors from 'cors';
import RouteLoggerInjector, { FakeRouteLoggerInjector } from './Common/Loggers/RouteLogger';
import CheckConfigs from './Configs/ConfigChecker';
import AdminAuthRoutes from './Routes/admin/auth/auth.routes';
import UserAuthRoutes from './Routes/auth/auth.routes';
import GlobalErrorHandlerRoute from './Routes/globalErrorHandler.routes';
import ServiceRoutes from './Routes/services/services.routes';


/**
 * Express App
 */
const app: express.Application = express();


/**
 * Configs
 */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


/**
 * Config Checker
 */
CheckConfigs();


/**
 * Fake route logger injection to prevent reference errors in routes that did not inject a RouteLogger
 */
app.use(FakeRouteLoggerInjector);


/**
 * Routes
 */
app.use('/api/auth', RouteLoggerInjector, UserAuthRoutes);
app.use('/api/services', RouteLoggerInjector, ServiceRoutes);
app.use('/api/admin/auth', RouteLoggerInjector, AdminAuthRoutes);


/**
 * Global Error Handler Route
 */
app.use(GlobalErrorHandlerRoute);


/**
 * Listen
 */
const PORT = process.env.PORT;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
// () Statuses
export class NotFoundError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'NotFoundError';
    }
}

export class InvalidError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'InvalidError';
    }
}

export class InternalServerError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = 'InternalServerError';
    }
}

// () DB
export class DBError extends Error {
    originalError: Error;
    constructor(message: string, originalError: Error) {
        super(message);
        this.name = 'DBError';
        this.originalError = originalError;
    }
}

// () Sanity
export class SanityError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'SanityError';
    }
}
import { IValidationRule } from "./Common/Validation/ValidationInterfaces";


/**
 * Validation Rules
 */

// Auth
const username: IValidationRule = {
    required: true,
    min: 2,
    max: 30
}
const password: IValidationRule = {
    required: true,
    min: 6,
    max: 30
}

// Service Category
const serviceCategoryTitle: IValidationRule = {
    required: true,
    min: 3,
    max: 100
}
const serviceCategoryDescription: IValidationRule = {
    required: true,
    min: 3,
    max: 1000
}

// Service
const serviceTitle: IValidationRule = {
    required: true,
    min: 3,
    max: 100
}
const serviceDescription: IValidationRule = {
    required: true,
    min: 3,
    max: 1000
}

/**
 * Export
 */
const ValidationRules = {
    username,
    password,
    serviceCategory: {
        title: {
            ...serviceCategoryTitle
        },
        description: {
            ...serviceCategoryDescription
        }
    },
    service: {
        title: {
            ...serviceTitle
        },
        description: {
            ...serviceDescription
        }
    }
}
export default ValidationRules;
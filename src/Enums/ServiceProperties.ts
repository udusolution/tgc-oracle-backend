export const ServiceModes = {
    Manual: 'manual',
    Auto: 'auto'
}

export const ServiceModesArray: string[] = Object.values(ServiceModes);
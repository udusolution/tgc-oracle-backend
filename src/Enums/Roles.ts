export const AdminRoles = {
    SuperAdmin: 'super_admin',
    Admin: 'admin',
    Moderator: 'moderator',
    VirtualAssistant: 'virtual_assistant'
}

export const AdminRolesArray = Object.values(AdminRoles);
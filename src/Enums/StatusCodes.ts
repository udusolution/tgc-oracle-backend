/**
 * @note enum to use, and type to apply to value in interfaces or the such
 */

export enum StatusCodes {
    Success = 200,
    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
    Invalid = 405,
    Sanity = 420,
    RefreshError = 477,
    ServerError = 500
}

export type StatusCodesType = 
    200 
    | 400
    | 401
    | 403
    | 404 
    | 405 
    | 420
    | 477
    | 500;
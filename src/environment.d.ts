/**
 * @description define the environment variables used in this node app
 */

import { MailServicesTypes } from "./Common/Services/Mail/Interfaces";
import { SMSServicesTypes } from "./Common/Services/SMS/Interfaces";

declare global {
	namespace NodeJS {
		interface ProcessEnv {
			NODE_ENV: 'development' | 'testing' | 'staging' | 'production';
			PORT?: string;
			DB_HOST: string;
			DB_USER: string;
			DB_PASS: string;
			DB_NAME: string;

			// JWT
			JWT_TOKEN_SECRET: string;
			JWT_REFRESH_SECRET: string;
			
			MAIL_SERVICE?: MailServicesTypes;
			// Mailer (MailGun)
			MAILGUN_USER?: string;
			MAILGUN_PASS?: string;
			MAILGUN_SENDER_EMAIL?: string;

			SMS_SERVICE?: SMSServicesTypes;
			// SMS Sender (Twilio)
			TWILIO_ACCOUNT_SID?: string;
			TWILIO_AUTH_TOKEN?: string;
			TWILIO_MESSAGING_SERVICE_SID?: string;

			// Hash
			HASH_SALT_ROUNDS?: string;

			// JWT
			JWT_TTL?: string;

		}
	}
}

export {}
/**
 * @description checks configurations such as ENV variables to make sure they exist
 */

import Config from "./Config";


/**
 * Create an error message for invalid configuration
 * @param tag configuration key
 * @param value current configuration value
 * @returns an error message
 */
function Fail(tag: string, value: any): string {
    return `Fatal Error: Missing the following config value: "${tag}". It's value is ${value}`;
}

/**
 * check the configurations of the process
 * in case the check fails, console messages are shown and the process exits with code 0
 */
function CheckConfigs() {

    try {

        const errors: string[] = [];

        // Check for faulty configurations
        if(!Config?.db?.host) errors.push(Fail('Config.db.host', Config?.db?.host));
        if(!Config?.db?.user) errors.push(Fail('Config.db.user', Config?.db?.user));
        if(!Config?.db?.name) errors.push(Fail('Config.db.name', Config?.db?.name));
        if(!Config?.jwt?.adminAccessTokenSecret) errors.push(Fail('Config.jwt.adminAccessTokenSecret', Config?.jwt?.adminAccessTokenSecret));
        if(!Config?.jwt?.adminRefreshTokenSecret) errors.push(Fail('Config.jwt.adminRefreshTokenSecret', Config?.jwt?.adminRefreshTokenSecret));
        if(!Config?.jwt?.userAccessTokenSecret) errors.push(Fail('Config.jwt.userAccessTokenSecret', Config?.jwt?.userAccessTokenSecret));
        if(!Config?.jwt?.userRefreshTokenSecret) errors.push(Fail('Config.jwt.userRefreshTokenSecret', Config?.jwt?.userRefreshTokenSecret));

        // ! If faulty configurations found, console log the problems and exit the process
        if(errors.length > 0) {
            errors.forEach(msg => console.error(msg));
            process.exit(0);
        }

    }
    catch(e) {
        process.exit(0);
    }
}


/**
 * Exports
 */
export default CheckConfigs;
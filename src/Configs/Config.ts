/**
 * @description general app configurations
 * @note relies on environment variables
 */


const Config = {
    mail: {
        user: '',
        password: '',
        mailerName: ''
    },
    jwt: {
        userAccessTokenSecret: process.env.JWT_TOKEN_SECRET,
        adminAccessTokenSecret: process.env.JWT_TOKEN_SECRET,
        userRefreshTokenSecret: process.env.JWT_REFRESH_SECRET,
        adminRefreshTokenSecret: process.env.JWT_REFRESH_SECRET,
        userRefreshTokenTTL: '30d',
        adminRefreshTokenTTL: '30d',
    },
    db: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        pass: process.env.DB_PASS,
        name: process.env.DB_NAME,
    }
}


/**
 * Exports
 */
export default Config;
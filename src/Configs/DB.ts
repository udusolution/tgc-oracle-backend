/**
 * @description database connection object for the app
 * @note uses 'mysql2' npm package
 */


import mysql from 'mysql2';
import Config from './Config';


// Create the connection pool. The pool-specific settings are the defaults
const DB = mysql.createPool({
    host:                 Config.db.host,
    user:                 Config.db.user,
    password:             Config.db.pass,
    database:             Config.db.name,
    waitForConnections:   true,
    connectionLimit:      10,
    queueLimit:           0,
    timezone:             '+00:00'
}).promise();


/**
 * Exports
 */
export default DB;
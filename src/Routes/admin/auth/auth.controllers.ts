import { NextFunction } from "express";
import { StatusCodes } from "../../../Enums/StatusCodes";
import { Req, Res } from "../../../Common/Interfaces/ExpressInterfaces";
import { RAddAdmin, RLogin, RRefresh } from "../../../Interfaces/RequestInterfaces";
import { XAccessToken, XTokens } from "../../../Interfaces/ResponseInterfaces";
import { IRouteLogger } from '../../../Common/Interfaces/LoggerInterfaces';
import { RefreshTokenError, UnauthorizedError } from "../../../Common/Auth/Errors";
import JWT from "../../../Common/Utils/JWT/JWT";
import Hash from "../../../Common/Utils/Hash/Hash";
import Config from "../../../Configs/Config";
import { Admin } from "../../../Models/Admin";
import AdminRepo from "../../../Repos/admin.repo";
import AdminRefreshRepo from "../../../Repos/adminRefresh.repo";
import { JwtPayload } from "../../../Common/Utils/JWT/Interfaces";
import { IId } from "../../../Interfaces/MiscInterfaces";
import { IAdmin } from "../../../Common/Auth/Interfaces";
import { InternalServerError, InvalidError } from "../../../Errors/Errors";
import FailureMessages from "../../../Messages/FailureMessages";


/**
 * Admin login
 */
export async function login(req: Req<RLogin>, res: Res<XTokens>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: RLogin = req.body;

    try {

        log.info(reqId, `calling (Admin)'findByUsername'`);
        // -> Get admin
        const admin: Admin | null = await AdminRepo.findByUsername(log, reqId, params.username);

        // Sanity checks, throw error in any of the following cases
        // ? If admin not found or is deleted
        if(!admin || admin.is_deleted) throw new UnauthorizedError();
        
        log.info(reqId, `calling (enc)'compare' to compare provided password with admin's password`);
        // -> Check if correct password
        const doesPasswordMatch: boolean = await Hash.compare(params.password, admin.password!);

        // -> If incorrect password, throw error
        if(!doesPasswordMatch) {
            log.info(reqId, `end [failure] ### incorrect password`);
            throw new UnauthorizedError();
        }

        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const accessToken: string = JWT.sign({ id: admin.id }, Config.jwt.adminAccessTokenSecret!);

        log.info(reqId, `calling (jwt)'generateRefreshToken'`);
        // -> Generate Refresh Token
        const refreshToken: string = JWT.sign({ id: admin.id }, Config.jwt.adminRefreshTokenSecret!, Config.jwt.adminRefreshTokenTTL!);

        log.info(reqId, `calling (Refresh)'add' to save generated refresh token into DB`);
        // -> Store Refresh Token
        await AdminRefreshRepo.add(log, reqId, refreshToken, admin.id);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { accessToken, refreshToken } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Generate new access token for the admin after checking the refresh token
 */
export async function refresh(req: Req<RRefresh>, res: Res<XAccessToken>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: RRefresh = req.body;

    try {

        log.info(reqId, `calling (AdminRefreshRepo)'exists'`);
        // -> Check if provided refresh token exists in DB
        const isExistingRefreshToken: boolean = await AdminRefreshRepo.exists(log, reqId, params.refreshToken);

        // Sanity checks, throw error in any of the following cases
        // ? If refresh token not found
        if(!isExistingRefreshToken) throw new RefreshTokenError();
        
        log.info(reqId, `calling (JWT)'verify' to decrypt the refresh token`);
        // -> Decrypt the refresh token
        const refreshTokenData: (JwtPayload & IId) = <JwtPayload & IId> JWT.verify(params.refreshToken, Config.jwt.adminRefreshTokenSecret!);

        // Sanity Checks, throw error in the following cases
        // ? If decrypted token does not have an admin ID
        if(!refreshTokenData.id) throw new RefreshTokenError();

        // Admin ID
        const adminId: number = refreshTokenData.id;

        log.info(reqId, `calling (AdminRepo)'findById' to check if he exists`);
        // -> Check if admin exists
        const admin: Admin | null = await AdminRepo.findById(log, reqId, adminId);

        // Sanity Checks, throw error in the following cases
        // ? If admin with ID in the refresh token was not found
        if(!admin) throw new RefreshTokenError();

        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const accessToken: string = JWT.sign({ id: adminId }, Config.jwt.adminAccessTokenSecret!);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { accessToken } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Get logged in admin's details
 */
export async function getMyDetails(req: Req<null>, res: Res<IAdmin>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    try {

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { ...req.admin! } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Add Admin
 */
export async function addAdmin(req: Req<RAddAdmin>, res: Res<IAdmin>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: RAddAdmin = req.body;

    try {

        log.info(reqId, `calling (AdminRepo)'isUsernameTaken'`);
        // -> Check if username taken
        const isUsernameTaken: boolean = await AdminRepo.isUsernameTaken(log, reqId, params.username);

        // ? If username taken, throw error
        if(isUsernameTaken) throw new InvalidError(FailureMessages.usernameTaken());

        log.info(reqId, `calling (AdminRepo)'isEmailTaken'`);
        // -> Check if email taken
        const isEmailTaken: boolean = await AdminRepo.isEmailTaken(log, reqId, params.email);

        // ? If email taken, throw error
        if(isEmailTaken) throw new InvalidError(FailureMessages.emailTaken());

        log.info(reqId, `calling (Hash)'hash'`);
        // -> Hash the password
        const passwordHashed: string = await Hash.hash(params.password);

        // New Admin Params
        const newAdminParams: RAddAdmin = {
            ...params,
            password: passwordHashed
        }

        log.info(reqId, `calling (AdminRepo)'addAdmin'`);
        // -> Add Admin
        const newAdminId: number = await AdminRepo.addAdmin(log, reqId, newAdminParams);

        log.info(reqId, `calling (AdminRepo)'findById' to get newly added admin`);
        // -> Get new admin
        const newAdmin: Admin | null = await AdminRepo.findById(log, reqId, newAdminId);

        // ? If new admin not found, throw error
        if(!newAdmin) throw new InternalServerError();

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { ...newAdmin } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Get all admins
 */
export async function getAdmins(req: Req<null>, res: Res<IAdmin[]>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    try {

        log.info(reqId, `calling (AdminRepo)'getAdmins'`);
        // -> Get admins
        const admins: Admin[] = await AdminRepo.getAdmins(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: admins });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

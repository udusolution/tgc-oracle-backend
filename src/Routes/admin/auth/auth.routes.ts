import express, { Router } from 'express';
import Auth from '../../../Common/Auth/Middleware';
import { AdminRoles } from '../../../Enums/Roles';
import * as Controllers from './auth.controllers';
import * as Validators from './auth.validators';
const router: Router = express.Router();


/**
 * Routes
 */
router.post('/login', Validators.login, Controllers.login);
router.post('/refresh', Validators.refresh, Controllers.refresh);
router.get('/getMyDetails', Auth.AdminAuth(), Controllers.getMyDetails);
router.post('/addAdmin', Auth.AdminAuth([AdminRoles.SuperAdmin, AdminRoles.Admin]), Validators.addAdmin, Controllers.addAdmin);
router.get('/getAdmins', Auth.AdminAuth(), Controllers.getAdmins);


/**
 * Exports
 */
const AdminAuthRoutes = router;
export default AdminAuthRoutes;
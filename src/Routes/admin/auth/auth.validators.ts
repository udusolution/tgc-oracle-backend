/**
 * @description validation middleware for admin auth
 */


import { body } from "express-validator";
import ValidationMessages from "../../../Messages/ValidationMessages";
import ValidationMiddleware from "../../../Common/Validation/ValidationMiddleware";
import { AdminRolesArray } from "../../../Enums/Roles";


export const login = ValidationMiddleware([
    body('username')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),

    body('password')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),
])

export const refresh = ValidationMiddleware([
    body('refreshToken')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
])

export const addAdmin = ValidationMiddleware([
    body('username')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),

    body('email')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isEmail()
            .withMessage(ValidationMessages.invalidEmail()),

    body('password')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),

    body('role')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isIn(AdminRolesArray)
            .withMessage(ValidationMessages.mustBeIn(AdminRolesArray)),
])

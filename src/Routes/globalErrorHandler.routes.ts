import { NextFunction } from 'express';
import { StatusCodes } from '../Enums/StatusCodes';
import { DBError, InternalServerError, InvalidError, NotFoundError, SanityError } from '../Errors/Errors';
import { Req, Res, XFailResponse } from '../Common/Interfaces/ExpressInterfaces';
import { IRouteLogger } from '../Common/Interfaces/LoggerInterfaces';
import { CustomValidationError } from '../Common/Validation/ValidationErrors';
import { JwtExpiredError, JwtVerifyError } from '../Common/Utils/JWT/Errors';
import { AuthError, ForbiddenError, MissingJwtSecretAuthError, RefreshTokenError, UnauthorizedError } from '../Common/Auth/Errors';
import FailureMessages from '../Messages/FailureMessages';


/**
 * Middleware to handle errors passed from routes.\
 * Returns a specific response for each custom error defined in the application.\
 * Will return a generic error response if the error thrown is not one of the defined custom errors.
 */
function GlobalErrorHandlerRoute(err: Error, req: Req<any>, res: Res<XFailResponse>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;
    const fnName: string = "GlobalErrorHandlerRoute";


    // () Not Found Error
    if(err instanceof NotFoundError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.NotFound).json({ message: err.message });
    }

    // () Invalid Error
    else if(err instanceof InvalidError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.Invalid).json({ message: err.message });
    }

    // () Sanity Error
    else if(err instanceof SanityError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.Sanity).json({ message: err.message });
    }

    // () Internal Server Error
    else if(err instanceof InternalServerError) {
        log.warn(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.ServerError).json({ message: err.message || FailureMessages.serverError() });
    }

    // () DB Error
    else if(err instanceof DBError) {
        log.warn(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.ServerError).json({ message: FailureMessages.serverError() });
    }

    // () Validation Error
    else if(err instanceof CustomValidationError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.BadRequest).json({ message: err.message, errors: err.validationErrors });
    }

    // () JWT Error
    else if(err instanceof JwtExpiredError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.Unauthorized).json({ message: 'authorization token expired' });
    }
    else if(err instanceof JwtVerifyError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.BadRequest).json({ message: err.message });
    }

    // () Auth Error
    else if(err instanceof UnauthorizedError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.Unauthorized).json();
    }
    else if(err instanceof ForbiddenError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.Forbidden).json();
    }
    else if(err instanceof MissingJwtSecretAuthError) {
        log.warn(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.ServerError).json({ message: FailureMessages.serverError() });
    }
    else if(err instanceof RefreshTokenError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.RefreshError).json();
    }
    else if(err instanceof AuthError) {
        log.info(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.Unauthorized).json({ message: err.message });
    }

    // () Other Error
    else {
        log.warn(reqId, `${fnName} ### message: "${err.message}", error: ${JSON.stringify(err)}`);
        res.status(StatusCodes.ServerError).json({ message: err.message });
    }

    // End
    next();
}



/**
 * Exports
 */
export default GlobalErrorHandlerRoute;
/**
 * @description validation middleware for admin auth
 */


import { body } from "express-validator";
import ValidationMessages from "../../Messages/ValidationMessages";
import ValidationMiddleware from "../../Common/Validation/ValidationMiddleware";
import ValidationRules from "../../ValidationRules";
import { ServiceModesArray } from "../../Enums/ServiceProperties";


export const addServiceCategory = ValidationMiddleware([
    body('title')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isLength({ min: ValidationRules.serviceCategory.title.min, max: ValidationRules.serviceCategory.title.max })
            .withMessage(ValidationMessages.mustBeMinMax(ValidationRules.serviceCategory.title.min, ValidationRules.serviceCategory.title.max)),

    body('description')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isLength({ min: ValidationRules.serviceCategory.description.min, max: ValidationRules.serviceCategory.description.max })
            .withMessage(ValidationMessages.mustBeMinMax(ValidationRules.serviceCategory.description.min, ValidationRules.serviceCategory.description.max)),
])

export const addService = ValidationMiddleware([
    body('title')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isLength({ min: ValidationRules.service.title.min, max: ValidationRules.service.title.max })
            .withMessage(ValidationMessages.mustBeMinMax(ValidationRules.service.title.min, ValidationRules.service.title.max)),

    body('description')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isLength({ min: ValidationRules.service.description.min, max: ValidationRules.service.description.max })
            .withMessage(ValidationMessages.mustBeMinMax(ValidationRules.service.description.min, ValidationRules.service.description.max)),

    body('service_category_id')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isInt()
            .withMessage(ValidationMessages.mustBeInt())
        .toInt(),

    body('mode')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isIn(ServiceModesArray),

    body('provider')
        .optional()
        .isString()
            .withMessage(ValidationMessages.mustBeString()),

    body('rate_per_1000')
        .exists({ checkNull: true, checkFalsy: false })
            .withMessage(ValidationMessages.required())
        .isFloat()
            .withMessage(ValidationMessages.mustBeNumber())
        .custom(val => val >= 0)
            .withMessage(ValidationMessages.mustBeZeroOrMore())
        .toFloat(),

    body('min_order')
        .exists({ checkNull: true, checkFalsy: false })
            .withMessage(ValidationMessages.required())
        .isInt()
            .withMessage(ValidationMessages.mustBeInt())
        .custom(val => val >= 0)
            .withMessage(ValidationMessages.mustBeZeroOrMore())
        .toInt(),

    body('max_order')
        .exists({ checkNull: true, checkFalsy: false })
            .withMessage(ValidationMessages.required())
        .isInt()
            .withMessage(ValidationMessages.mustBeInt())
        .custom((val, { req }) => val >= req.body.min_order)
            .withMessage('must be greater than "min_order"')
        .toInt(),

    body('increment')
        .exists({ checkNull: true, checkFalsy: false })
            .withMessage(ValidationMessages.required())
        .isInt()
            .withMessage(ValidationMessages.mustBeInt())
        .toInt(),

    body('overflow')
        .exists({ checkNull: true, checkFalsy: false })
            .withMessage(ValidationMessages.required())
        .isInt()
            .withMessage(ValidationMessages.mustBeInt())
        .toInt(),
])

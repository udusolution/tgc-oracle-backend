import { NextFunction } from "express";
import { StatusCodes } from "../../Enums/StatusCodes";
import { Req, Res } from "../../Common/Interfaces/ExpressInterfaces";
import { RLogin, RRefresh } from "../../Interfaces/RequestInterfaces";
import { XTokens } from "../../Interfaces/ResponseInterfaces";
import { IRouteLogger } from '../../Common/Interfaces/LoggerInterfaces';
import { UnauthorizedError } from "../../Common/Auth/Errors";
import JWT from "../../Common/Utils/JWT/JWT";
import Hash from "../../Common/Utils/Hash/Hash";
import Config from "../../Configs/Config";
import { Admin } from "../../Models/Admin";
import AdminRepo from "../../Repos/admin.repo";
import AdminRefreshRepo from "../../Repos/adminRefresh.repo";
import { JwtPayload } from "../../Common/Utils/JWT/Interfaces";
import { IId } from "../../Interfaces/MiscInterfaces";
import { IService, IServiceCategory, IServiceWithCategory } from "../../Interfaces/DataInterfaces";
import ServiceRepo from "../../Repos/service.repo";
import { ServiceCategory } from "../../Models/ServiceCategory";
import { InternalServerError, SanityError } from "../../Errors/Errors";
import FailureMessages from "../../Messages/FailureMessages";
import { Service } from "../../Models/Service";


/**
 * Add a new service category
 */
export async function addServiceCategory(req: Req<IServiceCategory>, res: Res<IServiceCategory>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: IServiceCategory = req.body;

    try {

        log.info(reqId, `calling (ServiceRepo)'addServiceCategory'`);
        // -> Add service category
        const newServiceCategoryId: number = await ServiceRepo.addServiceCategory(log, reqId, params);

        log.info(reqId, `calling (ServiceRepo)'getServiceCategory' to get newly created service category`);
        // -> Get new service category
        const newServiceCategory: ServiceCategory | null = await ServiceRepo.getServiceCategory(log, reqId, newServiceCategoryId);
        
        // Sanity checks, throw error in any of the following cases
        // ? If newly created service category not found
        if(!newServiceCategory) throw new InternalServerError();

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { ...newServiceCategory } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Add a new service
 */
export async function addService(req: Req<IService>, res: Res<IService>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: IService = req.body;

    try {

        log.info(reqId, `calling (ServiceRepo)'getServiceCategory' to check that service category exists`);
        // -> Check if service category exists 
        const serviceCategory: ServiceCategory | null = await ServiceRepo.getServiceCategory(log, reqId, params.service_category_id);
        
        // Sanity checks, throw error in any of the following cases
        // ? If service category not found
        if(!serviceCategory) throw new SanityError(FailureMessages.serviceCategoryNotFound());

        log.info(reqId, `calling (ServiceRepo)'addService'`);
        // -> Add service
        const newServiceId: number = await ServiceRepo.addService(log, reqId, params);

        log.info(reqId, `calling (ServiceRepo)'getService' to get newly created service`);
        // -> Get new service
        const newService: Service | null = await ServiceRepo.getService(log, reqId, newServiceId);
        
        // Sanity checks, throw error in any of the following cases
        // ? If newly created service not found
        if(!newService) throw new InternalServerError();

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { ...newService } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Get all service categories (without their services)
 */
export async function getServiceCategories(req: Req<null>, res: Res<IServiceCategory[]>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    try {

        log.info(reqId, `calling (ServiceRepo)'getServiceCategories'`);
        // -> Get service categories
        const serviceCategories: ServiceCategory[] = await ServiceRepo.getServiceCategories(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: serviceCategories });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Get all services (with their category information)
 */
 export async function getServicesWithCategoryInformation(req: Req<null>, res: Res<IServiceWithCategory[]>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    try {

        log.info(reqId, `calling (ServiceRepo)'getServicesWithCategoryInformation'`);
        // -> Get services
        const services: Service[] = await ServiceRepo.getServicesWithCategoryInformation(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: services });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

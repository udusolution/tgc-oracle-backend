import express, { Router } from 'express';
import Auth from '../../Common/Auth/Middleware';
import { AdminRoles } from '../../Enums/Roles';
import * as Controllers from './services.controllers';
import * as Validators from './services.validators';
const router: Router = express.Router();


/**
 * Routes
 */
router.post('/addServiceCategory', Auth.AdminAuth([AdminRoles.SuperAdmin, AdminRoles.Admin]), Validators.addServiceCategory, Controllers.addServiceCategory);
router.post('/addService', Auth.AdminAuth([AdminRoles.SuperAdmin, AdminRoles.Admin]), Validators.addService, Controllers.addService);
router.get('/getServiceCategories', Auth.HybridAuth(), Controllers.getServiceCategories);
router.get('/getServicesWithCategoryInformation', Auth.HybridAuth(), Controllers.getServicesWithCategoryInformation);


/**
 * Exports
 */
const ServiceRoutes = router;
export default ServiceRoutes;
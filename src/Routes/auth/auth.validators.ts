/**
 * @description validation middleware for user auth
 */


import { body } from "express-validator";
import ValidationMessages from "../../Messages/ValidationMessages";
import ValidationMiddleware from "../../Common/Validation/ValidationMiddleware";
import ValidationRules from "../../ValidationRules";


export const login = ValidationMiddleware([
    body('username')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),

    body('password')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),
])

export const refresh = ValidationMiddleware([
    body('refreshToken')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
])

export const addUser = ValidationMiddleware([
    body('username')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString()),

    body('email')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isEmail()
            .withMessage(ValidationMessages.mustBeString()),

    body('password')
        .exists({ checkNull: true, checkFalsy: true })
            .withMessage(ValidationMessages.required())
        .isString()
            .withMessage(ValidationMessages.mustBeString())
        .isLength({ min: ValidationRules.password.min, max: ValidationRules.password.max })
            .withMessage(ValidationMessages.mustBeMinMax(ValidationRules.password.min, ValidationRules.password.max))
])

import express, { Router } from 'express';
import Auth from '../../Common/Auth/Middleware';
import Config from '../../Configs/Config';
import { AdminRoles } from '../../Enums/Roles';
import * as Controllers from './auth.controllers';
import * as Validators from './auth.validators';
const router: Router = express.Router();


/**
 * Routes
 */
router.post('/login', Validators.login, Controllers.login);
router.post('/refresh', Validators.refresh, Controllers.refresh);
router.post('/addUser', Auth.AdminAuth([AdminRoles.SuperAdmin, AdminRoles.Admin]), Validators.addUser, Controllers.addUser);


/**
 * Exports
 */
const UserAuthRoutes = router;
export default UserAuthRoutes;
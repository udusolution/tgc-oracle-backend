import { NextFunction } from "express";
import { StatusCodes } from "../../Enums/StatusCodes";
import { Req, Res } from "../../Common/Interfaces/ExpressInterfaces";
import { RAddUser, RId, RLogin, RRefresh } from "../../Interfaces/RequestInterfaces";
import { XTokens } from "../../Interfaces/ResponseInterfaces";
import { IRouteLogger } from '../../Common/Interfaces/LoggerInterfaces';
import UserRepo from "../../Repos/user.repo";
import { User } from "../../Models/User";
import { UnauthorizedError } from "../../Common/Auth/Errors";
import JWT from "../../Common/Utils/JWT/JWT";
import Hash from "../../Common/Utils/Hash/Hash";
import Config from "../../Configs/Config";
import UserRefreshRepo from "../../Repos/userRefresh.repo";
import { IUser } from "../../Common/Auth/Interfaces";
import { InternalServerError, InvalidError, SanityError } from "../../Errors/Errors";
import FailureMessages from "../../Messages/FailureMessages";
import { IId } from "../../Interfaces/MiscInterfaces";
import { JwtPayload } from "../../Common/Utils/JWT/Interfaces";


/**
 * User Login
 */
export async function login(req: Req<RLogin>, res: Res<XTokens>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: RLogin = req.body;

    try {

        log.info(reqId, `calling (User)'findByEmail'`);
        // -> Get user
        const user: User | null = await UserRepo.findByUsername(log, reqId, params.username);

        // Sanity checks, throw error in any of the following cases
        // ? If user not found or is deleted or is banned
        if(!user || user.is_deleted || user.is_banned) throw new UnauthorizedError();
        
        log.info(reqId, `calling (enc)'compare' to compare provided password with user's password`);
        // -> Check if correct password
        const doesPasswordMatch: boolean = await Hash.compare(params.password, user.password!);

        // -> If incorrect password, throw error
        if(!doesPasswordMatch) {
            log.info(reqId, `end [failure] ### incorrect password`);
            throw new UnauthorizedError();
        }

        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const accessToken: string = JWT.sign({ id: user.id }, Config.jwt.userAccessTokenSecret!);

        log.info(reqId, `calling (jwt)'generateRefreshToken'`);
        // -> Generate Refresh Token
        const refreshToken: string = JWT.sign({ id: user.id }, Config.jwt.userRefreshTokenSecret!, Config.jwt.userRefreshTokenTTL!);

        log.info(reqId, `calling (Refresh)'add' to save generated refresh token into DB`);
        // -> Store Refresh Token
        await UserRefreshRepo.add(log, reqId, refreshToken, user.id);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { accessToken, refreshToken } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Generate new access token for the user after checking the refresh token
 */
export async function refresh(req: Req<RRefresh>, res: Res<XTokens>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: RRefresh = req.body;

    try {

        log.info(reqId, `calling (UserRefreshRepo)'exists'`);
        // -> Check if provided refresh token exists in DB
        const isExistingRefreshToken: boolean = await UserRefreshRepo.exists(log, reqId, params.refreshToken);

        // Sanity checks, throw error in any of the following cases
        // ? If refresh token not found
        if(!isExistingRefreshToken) throw new UnauthorizedError();
        
        log.info(reqId, `calling (JWT)'verify' to decrypt the refresh token`);
        // -> Decrypt the refresh token
        const refreshTokenData: (JwtPayload & IId) = <JwtPayload & IId> JWT.verify(params.refreshToken, Config.jwt.userRefreshTokenSecret!);

        // Sanity Checks, throw error in the following cases
        // ? If decrypted token does not have a user ID
        if(!refreshTokenData.id) throw new UnauthorizedError();

        // User ID
        const userId: number = refreshTokenData.id;

        log.info(reqId, `calling (UserRepo)'findById' to check if he exists`);
        // -> Check if user exists
        const user: User | null = await UserRepo.findById(log, reqId, userId);

        // Sanity Checks, throw error in the following cases
        // ? If user with ID in the refresh token was not found
        if(!user) throw new UnauthorizedError();

        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const accessToken: string = JWT.sign({ id: userId }, Config.jwt.userAccessTokenSecret!);

        log.info(reqId, `calling (jwt)'generateRefreshToken'`);
        // -> Generate Refresh Token
        const refreshToken: string = JWT.sign({ id: userId }, Config.jwt.userRefreshTokenSecret!, Config.jwt.userRefreshTokenTTL!);

        log.info(reqId, `calling (Refresh)'add' to save generated refresh token into DB`);
        // -> Store Refresh Token
        await UserRefreshRepo.add(log, reqId, refreshToken, userId);

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { accessToken, refreshToken } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

/**
 * Add User
 */
export async function addUser(req: Req<RAddUser>, res: Res<IUser>, next: NextFunction) {

    const log: IRouteLogger = req.log;
    const reqId: string = req.reqId;

    // Body Params
    const params: RAddUser = req.body;

    try {

        log.info(reqId, `calling (UserRepo)'isUsernameTaken'`);
        // -> Check if username taken
        const isUsernameTaken: boolean = await UserRepo.isUsernameTaken(log, reqId, params.username);

        // ? If username taken, throw error
        if(isUsernameTaken) throw new InvalidError(FailureMessages.usernameTaken());

        log.info(reqId, `calling (UserRepo)'isEmailTaken'`);
        // -> Check if email taken
        const isEmailTaken: boolean = await UserRepo.isEmailTaken(log, reqId, params.email);

        // ? If email taken, throw error
        if(isEmailTaken) throw new InvalidError(FailureMessages.emailTaken());

        log.info(reqId, `calling (Hash)'hash'`);
        // -> Hash the password
        const passwordHashed: string = await Hash.hash(params.password);

        // New User Params
        const newUserParams: RAddUser = {
            ...params,
            password: passwordHashed
        }

        log.info(reqId, `calling (UserRepo)'addUser'`);
        // -> Add User
        const newUserId: number = await UserRepo.addUser(log, reqId, newUserParams);

        log.info(reqId, `calling (UserRepo)'findById' to get newly added user`);
        // -> Get new user
        const newUser: IUser | null = await UserRepo.findById(log, reqId, newUserId);

        // ? If new user not found, throw error
        if(!newUser) throw new InternalServerError();

        // End
        log.info(reqId, `end [success]`);
        res.status(StatusCodes.Success).json({ message: 'success', data: { ...newUser } });
        next();

    }
    catch(e: any) {
        next(e);
    }
}

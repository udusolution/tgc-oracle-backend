import { OkPacket, RowDataPacket } from "mysql2";
import { IAdmin } from "../Common/Auth/Interfaces";
import { IRouteLogger } from "../Common/Interfaces/LoggerInterfaces";
import Mysql2Helpers from "../Common/Utils/Mysql2Helpers";
import DB from "../Configs/DB";
import { DBError } from "../Errors/Errors";
import { IService, IServiceCategory } from "../Interfaces/DataInterfaces";
import { Admin } from "../Models/Admin";
import { Service } from "../Models/Service";
import { ServiceCategory } from "../Models/ServiceCategory";


/**
 * add a new service category
 * @param params service category params
 * @returns the id of the created service category
 */
async function addServiceCategory(log: IRouteLogger, reqId: string, params: IServiceCategory): Promise<number> {

    const fnName: string = "ServiceRepo/addServiceCategory";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            INSERT INTO service_category
            SET ?
        `;

        // Params
        const insertParams = {
            title: params.title,
            description: params.description || ''
        }

        log.info(reqId, `${fnName} ### [query] insert`);
        // Query
        const [results] = await DB.query(sql, [insertParams]);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return (results as OkPacket).insertId;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * add a new service
 * @param params service params
 * @returns the id of the created service
 */
async function addService(log: IRouteLogger, reqId: string, params: IService): Promise<number> {

    const fnName: string = "ServiceRepo/addService";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            INSERT INTO service
            SET ?
        `;

        // Params
        const insertParams = {
            ...params
        }

        log.info(reqId, `${fnName} ### [query] insert`);
        // Query
        const [results] = await DB.query(sql, [insertParams]);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return (results as OkPacket).insertId;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get a service category without its services
 * @param id service category ID
 * @returns the service category
 */
 async function getServiceCategory(log: IRouteLogger, reqId: string, id: number): Promise<ServiceCategory | null> {

    const fnName: string = "ServiceRepo/getServiceCategory";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM service_category
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql, [id]);

        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Class
        const serviceCategory: IServiceCategory = <IServiceCategory> (results as Array<RowDataPacket>)[0];
        const serviceCategoryClass: ServiceCategory = new ServiceCategory(serviceCategory);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return serviceCategoryClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get a service without its category information
 * @param id service ID
 * @returns the service
 */
async function getService(log: IRouteLogger, reqId: string, id: number): Promise<Service | null> {

    const fnName: string = "ServiceRepo/getService";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM service
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql, [id]);

        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Class
        const service: IService = <IService> (results as Array<RowDataPacket>)[0];
        const serviceClass: Service = new Service(service);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return serviceClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get all the service categories without their services
 * @returns the service categories
 */
async function getServiceCategories(log: IRouteLogger, reqId: string): Promise<ServiceCategory[]> {

    const fnName: string = "ServiceRepo/getServiceCategories";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM service_category
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql);

        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return [];
        }

        // Class
        const serviceCategories: IServiceCategory[] = <IServiceCategory[]> (results as Array<RowDataPacket>);
        const serviceCategoriesClass: ServiceCategory[] = serviceCategories.map(sc => new ServiceCategory(sc));
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return serviceCategoriesClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


// IService with category title and description
interface IServiceWithCategoryDetails extends IService {
    categoryTitle: string;
    categoryDescription: string;
    categoryIsDeleted: boolean;
    categoryCreatedOn: string | Date;
    categoryUpdatedOn: string | Date;
}
/**
 * get all the services including their category information
 * @returns the services
 */
async function getServicesWithCategoryInformation(log: IRouteLogger, reqId: string): Promise<Service[]> {

    const fnName: string = "ServiceRepo/getServicesWithCategoryInformation";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                s.*,
                sc.title AS categoryTitle,
                sc.description AS categoryDescription,
                sc.is_deleted AS categoryIsDeleted,
                sc.createdOn AS categoryCreatedOn,
                sc.updatedOn AS categoryUpdatedOn
            FROM service AS s
            JOIN service_category AS sc ON s.service_category_id = sc.id
            WHERE s.is_deleted = 0
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql);

        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return [];
        }

        // Class
        const services: IServiceWithCategoryDetails[] = <IServiceWithCategoryDetails[]> (results as Array<RowDataPacket>);
        const servicesClass: Service[] = services.map(sc => new Service(sc, { 
            id: sc.service_category_id,
            title: sc.categoryTitle,
            description: sc.categoryDescription,
            is_deleted: sc.categoryIsDeleted,
            createdOn: sc.categoryCreatedOn,
            updatedOn: sc.categoryUpdatedOn
        }));
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return servicesClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


/**
 * Exports
 */
const ServiceRepo = {
    addServiceCategory,
    addService,
    getServiceCategory,
    getService,
    getServiceCategories,
    getServicesWithCategoryInformation
}
export default ServiceRepo;
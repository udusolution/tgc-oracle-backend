import { IRouteLogger } from "../Common/Interfaces/LoggerInterfaces";
import DB from "../Configs/DB";
import { DBError } from "../Errors/Errors";


/**
 * store user refresh token
 * @param refreshToken user's refresh token to
 * @param userId user ID
 */
async function add(log: IRouteLogger, reqId: string, refreshToken: string, userId: number): Promise<null> {

    const fnName: string = "UserRefreshRepo/add";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            INSERT INTO user_refresh
            SET ?
        `;

        // Params
        const params = {
            token: refreshToken,
            user_id: userId
        }

        log.info(reqId, `${fnName} ### [query] insert`);
        // Query
        await DB.query(sql, [params]);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * check if a user refresh token exists
 * @param token user's refresh token
 * @returns whether the token is found for this user or not
 */
async function exists(log: IRouteLogger, reqId: string, token: string): Promise<boolean> {

    const fnName: string = "UserRefreshRepo/exists";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT r.*
            FROM user_refresh AS r
            JOIN user AS u ON r.user_id = u.id
            WHERE r.token = ?
            AND u.is_deleted = 0
        `;

        log.info(reqId, `${fnName} ### [query] get token`);
        // Query
        const [results] = await DB.query(sql, [token]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return false;
        }
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return true;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


/**
 * Exports
 */
const UserRefreshRepo = {
    add,
    exists
}
export default UserRefreshRepo;
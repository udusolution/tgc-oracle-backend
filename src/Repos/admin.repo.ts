import { IAdmin } from "../Common/Auth/Interfaces";
import { IRouteLogger } from "../Common/Interfaces/LoggerInterfaces";
import Mysql2Helpers from "../Common/Utils/Mysql2Helpers";
import DB from "../Configs/DB";
import { DBError } from "../Errors/Errors";
import { RAddAdmin } from "../Interfaces/RequestInterfaces";
import { Admin } from "../Models/Admin";
import { OkPacket } from "mysql2";


/**
 * get an admin by its ID
 * @param id admin ID
 * @returns the found admin
 */
async function findById(log: IRouteLogger, reqId: string, id: number): Promise<Admin | null> {

    const fnName: string = "AdminRepo/findById";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM admin
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql, [id]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Extract
        const admin: IAdmin = <IAdmin> results[0];
        const adminClass: Admin = new Admin(admin);
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return adminClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get an admin by its username
 * @param username admin's username
 * @returns the found admin
 */
async function findByUsername(log: IRouteLogger, reqId: string, username: string): Promise<Admin | null> {

    const fnName: string = "AdminRepo/findByUsername";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM admin
            WHERE username = ?
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql, [username]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Extract
        const admin: IAdmin = <IAdmin> results[0];
        const adminClass: Admin = new Admin(admin);
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return adminClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * check if an admin has taken a specific email already
 * @param email admin's email
 * @returns whether it was taken or not
 */
async function isEmailTaken(log: IRouteLogger, reqId: string, email: string): Promise<boolean> {

    const fnName: string = "AdminRepo/isEmailTaken";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM admin
            WHERE email = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql, [email]);
        
        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [success, not found]`);
            return false;
        }
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return true;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * check if an admin has taken a specific username already
 * @param username admin's username
 * @returns whether it was taken or not
 */
async function isUsernameTaken(log: IRouteLogger, reqId: string, username: string): Promise<boolean> {

    const fnName: string = "AdminRepo/isUsernameTaken";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM admin
            WHERE username = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql, [username]);
        
        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [success, not found]`);
            return false;
        }
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return true;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * add a new admin
 * @param adminData admin's data
 * @returns new admin's ID
 */
 async function addAdmin(log: IRouteLogger, reqId: string, adminData: RAddAdmin): Promise<number> {

    const fnName: string = "AdminRepo/addAdmin";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            INSERT INTO admin
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add admin`);
        // Query
        const [results] = await DB.query(sql, [adminData]);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return (results as OkPacket).insertId;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get all admins
 * @returns all admins
 */
async function getAdmins(log: IRouteLogger, reqId: string): Promise<Admin[]> {

    const fnName: string = "AdminRepo/getAdmins";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM admin
        `;

        log.info(reqId, `${fnName} ### [query] get`);
        // Query
        const [results] = await DB.query(sql);
        
        // Check
        if(!Mysql2Helpers.selectHasRow(results)) {
            log.info(reqId, `${fnName} ### end [success, none found]`);
            return [];
        }

        // Extract
        const admins: IAdmin[] = <IAdmin[]> results;
        const adminClasses: Admin[] = admins.map(a => new Admin(a));
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return adminClasses;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


/**
 * Exports
 */
const AdminRepo = {
    findById,
    findByUsername,
    isEmailTaken,
    isUsernameTaken,
    addAdmin,
    getAdmins
}
export default AdminRepo;
import { OkPacket } from "mysql2";
import { IUser } from "../Common/Auth/Interfaces";
import { IRouteLogger } from "../Common/Interfaces/LoggerInterfaces";
import DB from "../Configs/DB";
import { DBError } from "../Errors/Errors";
import { RAddUser } from "../Interfaces/RequestInterfaces";
import { User } from "../Models/User";


/**
 * get a user by its ID
 * @param id user ID
 * @returns the found user
 */
async function findById(log: IRouteLogger, reqId: string, id: number): Promise<User | null> {

    const fnName: string = "UserRepo/findById";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get user by id`);
        // Query
        const [results] = await DB.query(sql, [id]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Extract
        const user: IUser = <IUser> results[0];
        const userClass: User = new User(user);
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return userClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get a user by its username
 * @param username user's username
 * @returns the found user
 */
async function findByUsername(log: IRouteLogger, reqId: string, username: string): Promise<User | null> {

    const fnName: string = "UserRepo/findByUsername";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE username = ?
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // Query
        const [results] = await DB.query(sql, [username]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Extract
        const user: IUser = <IUser> results[0];
        const userClass: User = new User(user);
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return userClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * get a user by its email
 * @param email user's email
 * @returns the found user
 */
async function findByEmail(log: IRouteLogger, reqId: string, email: string): Promise<User | null> {

    const fnName: string = "UserRepo/findByEmail";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE email = ?
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // Query
        const [results] = await DB.query(sql, [email]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return null;
        }

        // Extract
        const user: IUser = <IUser> results[0];
        const userClass: User = new User(user);
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return userClass;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * check if a user has taken a specific email already
 * @param email user's email
 * @returns whether it was taken or not
 */
 async function isEmailTaken(log: IRouteLogger, reqId: string, email: string): Promise<boolean> {

    const fnName: string = "UserRepo/isEmailTaken";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE email = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // Query
        const [results] = await DB.query(sql, [email]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [success, not found]`);
            return false;
        }
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return true;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * check if a user has taken a specific username already
 * @param username user's username
 * @returns whether it was taken or not
 */
 async function isUsernameTaken(log: IRouteLogger, reqId: string, username: string): Promise<boolean> {

    const fnName: string = "UserRepo/isUsernameTaken";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE username = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // Query
        const [results] = await DB.query(sql, [username]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [success, not found]`);
            return false;
        }
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return true;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


/**
 * add a new user
 * @param userData user's data
 * @returns new user's ID
 */
 async function addUser(log: IRouteLogger, reqId: string, userData: RAddUser): Promise<number> {

    const fnName: string = "UserRepo/addUser";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            INSERT INTO user
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add user`);
        // Query
        const [results] = await DB.query(sql, [userData]);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return (results as OkPacket).insertId;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


/**
 * Exports
 */
const UserRepo = {
    findById,
    findByUsername,
    findByEmail,
    isEmailTaken,
    isUsernameTaken,
    addUser
}
export default UserRepo;
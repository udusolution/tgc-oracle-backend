import { IRouteLogger } from "../Common/Interfaces/LoggerInterfaces";
import DB from "../Configs/DB";
import { DBError } from "../Errors/Errors";


/**
 * store admin refresh token
 * @param refreshToken admin's refresh token to
 * @param adminId admin ID
 */
async function add(log: IRouteLogger, reqId: string, refreshToken: string, adminId: number): Promise<null> {

    const fnName: string = "AdminRefreshRepo/add";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            INSERT INTO admin_refresh
            SET ?
        `;

        // Params
        const params = {
            token: refreshToken,
            admin_id: adminId
        }

        log.info(reqId, `${fnName} ### [query] insert`);
        // Query
        await DB.query(sql, [params]);
        
        // Return
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}

/**
 * check if an admin refresh token exists
 * @param token admin's refresh token
 * @returns whether the token is found for this admin or not
 */
async function exists(log: IRouteLogger, reqId: string, token: string): Promise<boolean> {

    const fnName: string = "AdminRefreshRepo/exists";
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT r.*
            FROM admin_refresh AS r
            JOIN admin AS a ON r.admin_id = a.id
            WHERE r.token = ?
            AND a.is_deleted = 0
        `;

        log.info(reqId, `${fnName} ### [query] get token`);
        // Query
        const [results] = await DB.query(sql, [token]);
        
        // Check
        if(!Array.isArray(results) || !results[0]) {
            log.info(reqId, `${fnName} ### end [not found]`);
            return false;
        }
        
        // Return
        log.info(reqId, `${fnName} ### end [success, found]`);
        return true;
    }
    catch(e: any) {
        log.error(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new DBError(e.message, e);
    }
}


/**
 * Exports
 */
const AdminRefreshRepo = {
    add,
    exists
}
export default AdminRefreshRepo;
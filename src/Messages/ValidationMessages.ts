const ValidationMessages = {
    required: () => `this property is required`,
    mustBeInt: () => `this property must be an integer`,
    mustBeString: () => `this property must be a string`,
    mustBeNumber: () => `this property must be a number`,
    mustBeMinMax: (min?: number, max?: number) => `this property's length must be at least ${min}${max ? ` and at most ${max}` : ''}`,
    mustBeZeroOrMore: () => `this property must be zero or greater`,
    mustBeIn: (arr: any[]) => `must be one of these values: [${arr.join(',')}]`,

    invalidEmail: () => `must be a valid email`
}

export default ValidationMessages;
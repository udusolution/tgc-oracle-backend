const FailureMessages = {
    usernameTaken: () => `This username is already taken`,
    emailTaken: () => `This email is already taken`,
    serverError: () => `Something went wrong, please try again later`,
    serviceCategoryNotFound: () => `Service category not found`,
}

export default FailureMessages;
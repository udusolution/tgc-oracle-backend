export class JwtError extends Error {
    originalError: any;
    constructor(message: string, originalError?: any) {
        super(message);
        this.name = "JwtError";
        this.originalError = originalError;
    }
}

export class JwtSignError extends JwtError {
    constructor(message: string, originalError?: any) {
        super(message);
        this.name = "JwtSignError";
        this.originalError = originalError;
    }
}

export class JwtVerifyError extends JwtError {
    constructor(message: string, originalError?: any) {
        super(message);
        this.name = "JwtVerifyError";
        this.originalError = originalError;
    }
}

export class JwtExpiredError extends JwtVerifyError {
    constructor(message: string, originalError?: any) {
        super(message);
        this.name = "JwtExpiredError";
        this.originalError = originalError;
    }
}
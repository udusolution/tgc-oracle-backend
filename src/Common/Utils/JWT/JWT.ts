/**
 * @description utility module for JWT
 * @note uses 'jsonwebtoken' npm package
 * 
 * Environment Variables
 * JWT_TTL (optional, must be zeit/ms format, eg "2 days", "10h", "7d")
 */


import * as jwt from 'jsonwebtoken';
import { JwtExpiredError, JwtSignError, JwtVerifyError } from './Errors';
import ms from 'ms';
import { JwtPayload } from './Interfaces';


/**
 * generate a JWT user access token
 * @param payload JWT payload
 * @param secret JWT secret key
 * @param ttl token's Time-To-Live, expressed in seconds or a string describing a time span zeit/ms. Eg: "2 days", "10h", "7d"
 * @throws {JwtSignError}
 * 
 * @note the TTL used is (by presidency) either the "ttl" parameter, the ENV variable "JWT_TTL", or a default value of '15m'
 */
function sign(payload: string | object | Buffer, secret: string, ttl?: string): string {

    try {

        // ! Sanity Checks, throw errors in the following cases
        // ! If secret is not passed
        // ! If ttl is not valid format
        if(!secret)         throw new JwtSignError(`secret parameter is missing, got ${secret}`);
        if(ttl && !ms(ttl)) throw new JwtSignError(`ttl parameter must be of valid format, got ${secret}`);

        // TTL
        const envTTL: string | undefined = process.env.JWT_TTL;
        const TTL: string = ttl || envTTL || '15m';
        
        // -> Generate the JWT token
        const token: string = jwt.sign(payload, secret, { expiresIn: TTL });

        // End
        return token;
    }
    catch(e: any) {
        throw new JwtSignError(e?.message, e);
    }
}


/**
 * verify a JWT token and return its decrypted payload
 * @param token user JWT access token to decrypt
 * @param secret secret used to sign this JWT token
 * @throws {JwtExpiredError}
 * @throws {JwtVerifyError} generic
 */
function verify(token: string, secret: string): JwtPayload {

    try {

        // ! Sanity Checks, throw errors in the following cases
        // ! If secret is not passed
        if(!secret) throw new JwtSignError(`secret parameter is missing, got ${secret}`);

        // -> Decrypt the JWT token
        // @ts-ignore jwt.verify would always return a payload and not a string in our use case
        const payload: JwtPayload = jwt.verify(token, secret);

        // End
        return payload;
    }
    catch(e: any) {

        // () JWT Expired Error
        if(e instanceof jwt.TokenExpiredError) {
            throw new JwtExpiredError(e?.message, e);
        }

        // () Other JWT Error
        throw new JwtVerifyError(e?.message, e);
    }
}


/**
 * Exports
 */
const JWT = {
    sign,
    verify
}
export default JWT;
/**
 * @description utility module to encrypt/decrypt/hash/compare text
 * @note uses 'bcrypt' npm package
 * 
 * Environment Variables:
 * HASH_SALT_ROUNDS (optional)
 */


import * as bcrypt from 'bcrypt';
import { HashError } from './Errors';


/**
 * hash a piece of data
 * @param dataToHash 
 * @note to override the default salt rounds (10), add an environment variable "HASH_SALT_ROUNDS"
 */
async function hash(dataToHash: string | Buffer): Promise<string> {

    try {

        // Salt rounds
        const envSaltRounds: string | undefined = process.env.HASH_SALT_ROUNDS;
        const saltRounds: number = envSaltRounds && !isNaN(parseInt(envSaltRounds)) 
            ? parseInt(envSaltRounds) 
            : 10;

        // -> Hash the data
        const hashedStr: string = await bcrypt.hash(dataToHash, saltRounds);

        // End
        return hashedStr;
    }
    catch(e: any) {
        throw new HashError(e?.message, e);
    }
}


/**
 * compare a clear string/buffer and a hash string to see if they match
 * @param clearData un-hashed string or Buffer
 * @param hashedStr hash string
 */
async function compare(clearData: string | Buffer, hashedStr: string): Promise<boolean> {

    try {

        // -> Compare the clear and hashed strings
        const doesMatch: boolean = await bcrypt.compare(clearData, hashedStr);

        // End
        return doesMatch;
    }
    catch(e: any) {
        throw new HashError(e?.message, e);
    }
}


/**
 * Exports
 */
const Hash = {
    hash,
    compare
}
export default Hash;
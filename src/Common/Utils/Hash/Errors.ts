export class HashError extends Error {
    originalError?: any;
    constructor(message: string, originalError?: any) {
        super(message);
        this.name = "HashError";
        this.originalError = originalError;
    }
}
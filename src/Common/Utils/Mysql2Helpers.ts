function selectHasRow(results: any): boolean {
    return (
        results
        && Array.isArray(results)
        && results[0]
    )
}


/**
 * Exports
 */
const Mysql2Helpers = {
    selectHasRow
}
export default Mysql2Helpers;
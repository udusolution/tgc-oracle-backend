/**
 * @description utility functions for strings
 */


import moment, { Moment, MomentInput } from 'moment';


/**
 * get the difference between two date times in "Xh Xm" format
 * @param from starting date time
 * @param to end date time
 */
function timeDifference(from: MomentInput, to: MomentInput): string {

    // Define the moment objects
    const fromDate: Moment = moment(from);
    const toDate: Moment = moment(to);

    // ! Check parameter validity
    if(!fromDate.isValid() || !toDate.isValid) return 'Invalid date';

    // Get difference in minutes between the dates
    const durationInMinutes: number = toDate.diff(fromDate, 'minutes');

    // Get duration difference in hours to be shown
    const displayedHours: number = durationInMinutes > 59 ? Math.floor(durationInMinutes / 60) : 0;

    // Get duration difference in minutes to be shown
    const displayedMinutes: number = durationInMinutes <= 59 ? durationInMinutes : durationInMinutes % 60;

    // Compute the displayed time
    const durationStr = `${displayedHours ? `${displayedHours}h ` : ''}${displayedMinutes}m`;

    // End
    return durationStr;
}


/**
 * format a number into USA phone (###) ###-####
 * @param str phone number as a numeric string
 */
function formatPhone(str: string): string {

    // ! If empty or not string, return empty
    if(!str || typeof str !== 'string' || str.length === 0) return ``;

    // Strip the input and leave it to be numbers only
    const sanitizedInput: string = str.replace(/[^0-9]/g, '');

    // If str < 3: (###)
    if(sanitizedInput.length <= 3) return `(${sanitizedInput})`;

    // If str is between 3 & 6 numbers: (###) ###
    if(sanitizedInput.length <= 6) return `(${sanitizedInput.substr(0, 3)}) ${sanitizedInput.substr(3)}`;

    // If str is more than 6 numbers: (###) ###-####
    return `(${sanitizedInput.substr(0, 3)}) ${sanitizedInput.substr(3,3)}-${sanitizedInput.substr(6,4)}`;
}


/**
 * format a string to be SSN format
 * @param str string to format
 */
function formatSSN(str: string): string {

    // ! If empty or not string, return empty
    if(!str || typeof str !== 'string' || str.length === 0) return ``;

    // Strip the input and leave it to be numbers only
    const sanitizedInput: string = str.replace(/[^0-9]/g, '');

    // If str < 3: ###
    if(sanitizedInput.length <= 3) return sanitizedInput;

    // If str is between 3 & 5 numbers: ###-##
    if(sanitizedInput.length <= 5) return `${sanitizedInput.substr(0, 3)}-${sanitizedInput.substr(3)}`;

    // If str is between 6 & 8 numbers: ###-##-####
    if(sanitizedInput.length < 9) return `${sanitizedInput.substr(0, 3)}-${sanitizedInput.substr(3, 2)}-${sanitizedInput.substr(5)}`;

    // If str is more than 8 numbers: ###-##-####
    return `${sanitizedInput.substr(0, 3)}-${sanitizedInput.substr(3, 2)}-${sanitizedInput.substr(5, 4)}`;
}


/**
 * Exports
 */
const StringUtils = {
    timeDifference,
    formatPhone,
    formatSSN
}
export default StringUtils;
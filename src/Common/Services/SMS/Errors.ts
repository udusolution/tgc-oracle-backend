export class SMSError extends Error {
    originalError: Error | undefined;
    constructor(message: string, originalError?: Error) {
        super(message);
        this.name = "SMSError";
        this.originalError = originalError;
    }
}

export class SMSConfigError extends SMSError {
    constructor(message: string) {
        super(message);
        this.name = "SMSConfigError";
    }
}

export class SMSInvalidPhoneNumberError extends SMSError {
    constructor(message: string, originalError?: any) {
        super(message);
        this.name = "SMSInvalidPhoneNumberError";
        this.originalError = originalError;
    }
}
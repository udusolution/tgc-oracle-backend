/**
 * @description module that sends sms
 * 
 * Environment variables for the used implementations:
 * Twilio:
 * _TWILIO_ACCOUNT_SID
 * _TWILIO_AUTH_TOKEN
 * _TWILIO_MESSAGING_SERVICE_SID
 */


import { IRouteLogger } from "../../Interfaces/LoggerInterfaces";
import TwilioSMS from "./Implementations/SMS.twilio";
import { SMSError } from "./Errors";
import { ISMSSender, ISMSOptions, SMSServices, SMSServicesTypes } from "./Interfaces";


// Default Options
const defaultOptions = {
    service: SMSServices.Twilio
}


/**
 * send an email
 * @note each service implementation requires its own environment variables
 * @param log logger
 * @param reqId request ID
 * @param to phone number to send sms to
 * @param body the sms message
 * @param service the sms service to use, defaults to the selected one in the "SMS_SERVICE" ENV, else to "twilio"
 * @throws {SMSError}
 * @throws {SMSConfigError}
 * @throws {SMSInvalidPhoneNumberError}
 * 
 * @example send(log, reqId, '+12223334444', 'Hello')
 */
async function SendSMS(
    log: IRouteLogger,
    reqId: string,
    to: string,
    body: string,
    options: ISMSOptions = defaultOptions
): Promise<void>{

    const fnName = "sms/send";
    log.info(reqId, `${fnName} ### start`);

    try {

        const serviceToUse: SMSServicesTypes = options.service || process.env.SMS_SERVICE || defaultOptions.service;
        
        log.info(reqId, `${fnName} ### get implementation of service "${serviceToUse}"`);
        // -> Get Implementation
        const sender: ISMSSender = getSmsSender(serviceToUse);

        log.info(reqId, `${fnName} ### sending sms`);
        // -> Send SMS
        await sender.send(to, body);
    
        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
    }
    catch(e: any) {
        log.info(reqId, `${fnName} ### end [failure] ### message: "${e?.message}", error: ${JSON.stringify(e)}`);

        // () SMS Error
        if(e instanceof SMSError) throw e;

        // () Other
        throw new SMSError(e?.message, e);
    }


}


/**
 * get the SMS service implementation based on the chosen service
 * @param service chosen service
 */
function getSmsSender(service: SMSServicesTypes): ISMSSender {
    switch(service) {
        case SMSServices.Twilio:
            return new TwilioSMS();
        default:
            return new TwilioSMS();
    }
}


/**
 * Exports
 */
export default SendSMS;
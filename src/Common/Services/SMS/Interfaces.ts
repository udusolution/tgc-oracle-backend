// SMS Sender
export interface ISMSSender {
    send(to: string, body: string): Promise<void>;
}

// SMS Options
export interface ISMSOptions {
    service?: SMSServicesTypes;
}

// SMS Services Implemented
export enum SMSServices {
    Twilio = 'twilio'
}
export type SMSServicesTypes = 
    'twilio';
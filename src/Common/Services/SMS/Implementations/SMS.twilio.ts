/**
 * @description implementation of SMS sending using "twilio"
 */


import * as twilio from 'twilio';
import { SMSConfigError, SMSError, SMSInvalidPhoneNumberError } from '../Errors';
import { ISMSSender } from '../Interfaces';


/**
 * Class that implements sending SMS using the Twilio service
 * @requires process.env.TWILIO_ACCOUNT_SID
 * @requires process.env.TWILIO_AUTH_TOKEN
 * @requires process.env.TWILIO_MESSAGING_SERVICE_SID
 */
export default class TwilioSMS implements ISMSSender {

    // Properties
    accountSid: string;
    authToken: string;
    messagingServiceSid: string;
    client: twilio.Twilio;

    // Constructor
    constructor() {

        // Environment Variables
        const accountSid: string | undefined            = process.env.TWILIO_ACCOUNT_SID;
        const authToken: string | undefined             = process.env.TWILIO_AUTH_TOKEN;
        const messagingServiceSid: string | undefined   = process.env.TWILIO_MESSAGING_SERVICE_SID;

        // Check Environment Variables
        if(!accountSid)             throw new SMSConfigError('Twilio "twilio.accountSid" configuration missing');
        if(!authToken)              throw new SMSConfigError('Twilio "twilio.authToken" configuration missing');
        if(!messagingServiceSid)    throw new SMSConfigError('Twilio "twilio.messagingServiceSid" configuration missing');

        // Fill the Configuration Variables
        this.accountSid = accountSid;
        this.authToken = authToken;
        this.messagingServiceSid = messagingServiceSid;

        // Initialize Client
        try {
            this.client =  new twilio.Twilio(accountSid, authToken);
        }
        catch(e: any) {
            throw new SMSConfigError(e?.message);
        }
    }

    /**
     * send SMS using Twilio
     * @param to phone number to send to
     * @param body SMS body
     */
    async send(to: string, body: string): Promise<void> {
        try {
            await this.client.messages.create({ 
                messagingServiceSid: this.messagingServiceSid,
                to: to,
                body: body,
            })
        }
        catch(e: any) {

            // () Invalid Phone Number Twilio error
            if(e?.code == 21211) {
                throw new SMSInvalidPhoneNumberError(`provided phone number "${to}" is invalid`, e);
            }

            // () Generic Sms Error
            throw new SMSError(e?.message, e);
        }
    }
}
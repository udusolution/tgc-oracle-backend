export class MailError extends Error {
    originalError: Error | undefined;
    constructor(message: string, originalError?: Error) {
        super(message);
        this.name = "MailError";
        this.originalError = originalError;
    }
}

export class MailConfigError extends MailError {
    constructor(message: string) {
        super(message);
        this.name = "MailConfigError";
    }
}
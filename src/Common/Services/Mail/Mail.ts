/**
 * @description module that sends emails
 * @note uses "nodemailer" npm package and "Mailgun" service
 * @note mailer configurations must be specified in the "Configs/Config" object
 * @dependency RouteLogger
 * 
 * Environment variables for the used implementations:
 * MailGun:
 * _MAILGUN_USER
 * _MAILGUN_PASS
 * _MAILGUN_SENDER_EMAIL
 */


import { IRouteLogger } from '../../Interfaces/LoggerInterfaces';
import { MailConfigError, MailError } from './Errors';
import { IMailOptions, IMailSender, MailServices, MailServicesTypes } from './Interfaces';
import MailGunEmailer from './Implementations/Mail.MailGun';

// Default Options
const defaultMailSenderOptions = {
    attachments: [],
    service: MailServices.MailGun
}


/**
 * send an email
 * @param log logger
 * @param reqId request ID
 * @param to email(s) to send this email to (comma separated)
 * @param subject the subject of the email
 * @param body the html body of the email
 * @param options any attachments, and also the service to be used, defaults to the one specified in the ENV, else MailGun
 * @throws {MailError | MailConfigError}
 * 
 * @example send(log, reqId, 'example1@mail.com,example2@mail.com', 'Test Email', '<p>Hi</p>', [{ filename: 'text3.txt', path: '/path/to/file.txt' }])
 */
async function SendEmail(
    log: IRouteLogger,
    reqId: string,
    to: string,
    subject: string,
    body: string,
    options: IMailOptions = defaultMailSenderOptions
){

    const fnName: string = "Mail/send";
    log.info(reqId, `${fnName} ### start`);

    try {

        // -> Service to use, else MailGun if not specified
        const serviceToUse: MailServicesTypes = options.service || process.env.MAIL_SERVICE || defaultMailSenderOptions.service;

        log.info(reqId, `${fnName} ### creating mailer for service "${serviceToUse}"`);
        // -> Get mail sender implementation
        const mailer: IMailSender = getMailSender(serviceToUse);

        log.info(reqId, `${fnName} ### sending email`);
        // -> Send mail using defined transport object
        await mailer.send(to, subject, body, options.attachments);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        
    }
    catch(e: any) {
        log.warn(reqId, `${fnName} ### end [failure] ### message: "${e?.message}", error: ${JSON.stringify(e)}`);

        // () Mail Error
        if(e instanceof MailError) throw e;

        // () Other Error
        throw new MailError(e?.message, e);
    }

    

}


/**
 * get the SMS service implementation based on the chosen service
 * @param service chosen service
 */
function getMailSender(service: MailServicesTypes): IMailSender {
    switch(service) {
        case MailServices.MailGun:
            return new MailGunEmailer();
        default:
            return new MailGunEmailer();
    }
}


/**
 * Exports
 */
export default SendEmail;
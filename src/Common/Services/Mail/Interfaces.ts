// Email Attachment
export interface IMailAttachment {
    filename: string,
    path: string
}

// Email Options
export interface IMailOptions {
    // mail service to use
    service?: MailServicesTypes;
    // email attachments
    attachments?: IMailAttachment[];
}

// Mail Sender
export interface IMailSender {
    send(to: string, subject: string, body: string, attachments?: IMailAttachment[]): Promise<void>;
}

// Mail Services Implemented
export enum MailServices {
    MailGun = 'mailgun'
}
export type MailServicesTypes = 
    'mailgun';
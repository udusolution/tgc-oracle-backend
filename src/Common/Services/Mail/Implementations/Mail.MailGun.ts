/**
 * @description implementation of SMS sending using "twilio"
 */

import { createTransport, Transporter } from "nodemailer";
import { MailOptions } from "nodemailer/lib/json-transport";
import { SMSConfigError } from "../../SMS/Errors";
import { MailConfigError, MailError } from "../Errors";
import { IMailAttachment, IMailSender } from "../Interfaces";


/**
 * Class that implements sending SMS using the Twilio service
 * @requires process.env.MAILGUN_USER
 * @requires process.env.MAILGUN_PASS
 * @requires process.env.MAILGUN_SENDER_EMAIL
 */
export default class MailGunEmailer implements IMailSender {

    // Properties
    user: string;
    pass: string;
    senderEmail: string;
    transporter: Transporter;

    // Constructor
    constructor() {

        // Environment Variables
        const user: string | undefined          = process.env.MAILGUN_USER;
        const pass: string | undefined          = process.env.MAILGUN_PASS;
        const senderEmail: string | undefined   = process.env.MAILGUN_SENDER_EMAIL;

        // Check Environment Variables
        if(!user)           throw new MailConfigError('required MailGun environment variable MAILGUN_USER missing');
        if(!pass)           throw new MailConfigError('required MailGun environment variable MAILGUN_PASS missing');
        if(!senderEmail)    throw new MailConfigError('required MailGun environment variable MAILGUN_SENDER_EMAIL missing');

        // Set Configuration Properties
        this.user = user;
        this.pass = pass;
        this.senderEmail = senderEmail;

        // Create the Transporter
        try {
            const transporter: Transporter = createTransport({
                service: 'Mailgun',
                auth: {
                    user,
                    pass,
                },
                tls: {
                    // do not fail on invalid certs
                    rejectUnauthorized: false
                },
            });
            
            this.transporter = transporter;
        }
        catch(e: any) {
            throw new SMSConfigError(e?.message);
        }

    }


    /**
     * send email using MailGun
     * @param to email address to send to
     * @param subject email subject
     * @param body email HTML body
     * @param attachments email attachments
     */
    async send(to: string, subject: string, body: string, attachments?: IMailAttachment[]): Promise<void> {
        
        try {

            // -> Setup email data with unicode symbols
            const mailOptions: MailOptions = {
                from: `${this.senderEmail} <${this.user}>`,
                to: to,
                subject: subject,
                html: body
            };
            if(attachments) mailOptions.attachments = attachments;

            // -> Send mail using defined transport object
            await this.transporter.sendMail(mailOptions);
            
        }
        catch(mailError: any) {
            throw new MailError(mailError?.message, mailError);
        }
    }
}
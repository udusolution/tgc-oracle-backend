/**
 * Description:
 *      Holds a dictionary of loggers that log every request of each service into its own log file
 * Application:
 *      In each route.
 * Notes:   
 *      There is a max log file size set and a max number of log files per service. 
 *      Automatic cleanup occurs when size is exceeded, old log files are deleted.
 * 			!!! make sure to create the log file folders beforehand !!!
 */


import { NextFunction } from 'express';
import { Format } from 'logform';
import moment from 'moment';
import * as uuid from 'uuid';
import { createLogger, format, Logger, transports } from 'winston';
import 'winston-daily-rotate-file';
import { Req, Res } from '../Interfaces/ExpressInterfaces';
import { IRouteLogger } from '../Interfaces/LoggerInterfaces';
import { Request } from 'express-serve-static-core';


/**
 * Loggers dictionary (singleton design pattern)
 */
let loggers: Record<string, IRouteLogger> = {};


/**
 * checks if service logger has been created or not. Creates service logger if not created
 * @param {string} servicePath route path of the service
 * @note uses an object defined above called 'loggers' to store created loggers
 */
function getOrCreateLogger(route: string): IRouteLogger {
    let logger: IRouteLogger = loggers[route];
    if(!logger) {
        logger = create(route);
        loggers[route] = logger;
    }
    return logger;
}


/**
 * format of each log line, used in 'createLogger' below
 * @param route 
 * @returns 
 */
const logFormat = function(route: string): Format {
    return format.printf(function({ level, message, label, timestamp }) {
        return `${timestamp} [${level}]: ${route} ### ${message}`;
    })
};


/**
 * winston error logger for logging error logs in a separate file for easy error checking
 */
const errorLogger: Logger = createLogger({
    transports: [
        new transports.DailyRotateFile({
            dirname: 'logs/errors',
            filename: 'errors-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: false,
            maxSize: '20m',
            maxFiles: '14d',
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                logFormat('errors')
            )
        }),
    ]
});


/**
 * create a route logger
 * @param {string} route route path of the service
 * @note the logger created is a winston logger instance, but the logger returned is a wrapper
 */
function create(route: string): IRouteLogger {

    // Get route parts
    const routeParts: Array<string> = route.split('/');

    // Get route name
    const routeName: string | undefined = routeParts[routeParts.length - 1];
    console.log(routeParts, routeName)

    // ! If no route detected, return a fake logger to avoid reference errors
    if(!routeName) return FakeRouteLogger();

    // Form the log file path and name
    const dir: string = `logs/${route}/`;
    const filename: string = `${routeName}-%DATE%.log`;

    // Console log the logger file initialization
    console.info(`logger initialization for: ${dir}${routeName}-${moment().format('YYYY-MM-DD')}.log`);

    // Initialize the winston logger
    const winstonLogger: Logger = createLogger({
        transports: [
            new transports.DailyRotateFile({
                dirname: dir,
                filename: filename,
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
                maxSize: '20m',
                maxFiles: '30d',
                format: format.combine(
                    format.timestamp({
                        format: 'YYYY-MM-DD HH:mm:ss'
                    }),
                    logFormat(route)
                )
            }),
        ]
    });

    // Wrapper logger
    const logger: IRouteLogger = {
        log: function(reqId: string, level: string, message: string): void {
            winstonLogger.log(level, `${reqId} ### ${message}`);
        },
        error: function(reqId: string, message: string): void {
            winstonLogger.error(`${reqId} ### ${message}`);
        },
        warn: function(reqId: string, message: string): void {
            winstonLogger.warn(`${reqId} ### ${message}`);
            errorLogger.warn(`${reqId} ### ${message}`);
            console.warn(`${route} ### ${reqId} ### ${message}`);
        },
        verbose: function(reqId: string, message: string): void {
            winstonLogger.verbose(`${reqId} ### ${message}`);
        },
        info: function(reqId: string, message: string): void {
            winstonLogger.info(`${reqId} ### ${message}`);
        },
        debug: function(reqId: string, message: string): void {
            winstonLogger.debug(`${reqId} ### ${message}`);
        },
        silly: function(reqId: string, message: string): void {
            winstonLogger.silly(`${reqId} ### ${message}`);
        }
    };
    
    // End
    return logger;
}


/**
 * creates a fake logger that logs nowhere, in case needed in place of an actual logger
 */
function FakeRouteLogger(): IRouteLogger {
    return {
        log: function(reqId: string, level: string, message: string): void {},
        error: function(reqId: string, message: string): void {},
        warn: function(reqId: string, message: string): void {},
        verbose: function(reqId: string, message: string): void {},
        info: function(reqId: string, message: string): void {},
        debug: function(reqId: string, message: string): void {},
        silly: function(reqId: string, message: string): void {}
    }
}


/**
 * middleware that adds a logger to the request object for the endpoint being called
 */
function RouteLoggerInjector(req: Req<any>, res: Res<any>, next: NextFunction) {

    // Generate unique request ID
    const reqId: string = uuid.v4();

    // Get route full path
    const fullPath: string = req.baseUrl + req.path;

    // Get or create the logger
    const logger: IRouteLogger = getOrCreateLogger(fullPath);

    // Inject the reqId, logger, and fullPath into the express req object
    req.reqId = reqId;
    req.fullPath = fullPath;
    req.log = logger;

    // Log the start line
    logger.info(reqId, `start ### request body: ${JSON.stringify(req.body)}`);

    // End
    next();
}


/**
 * middleware that adds a fake logger to the request object
 * @note useful to inject before any routes so that a route that did not inject an actual RouteLogger will not throw reference errors
 */
function FakeRouteLoggerInjector(req: Req<any>, res: Res<any>, next: NextFunction) {

    // Placeholder request ID
    const reqId = `N/A`;

    // Get route full path
    const fullPath: string = req.baseUrl + req.path;

    // Create fake logger
    const logger: IRouteLogger = FakeRouteLogger();

    // Inject the reqId, logger, and fullPath into the express req object
    req.reqId = reqId;
    req.fullPath = fullPath;
    req.log = logger;

    // Log the start line
    logger.info(reqId, 'start ###');

    // End
    next();
}



/**
 * Exports
 */
export default RouteLoggerInjector;
export {
    FakeRouteLogger,
    FakeRouteLoggerInjector
}
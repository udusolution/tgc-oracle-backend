export interface IValidationRule {
    required?: boolean,
    min?: number,
    max?: number
}
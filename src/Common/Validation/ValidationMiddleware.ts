/**
 * @description handy wrapper that appends the validation results checker middleware to the validation chain
 */


import { ValidationChain } from "express-validator";
import ValidationChecker from "./ValidationChecker";


/**
 * returns a chain of middleware that validate request fields, and appends the ValidationChecker middleware at the end which takes the request to the global error handler if validation errors occurred
 * @param chain "express-validator" ValidationChain array
 * @returns
 */
function ValidationMiddleware(chain: Array<ValidationChain> = []) {
    return [
        ...chain,
        ValidationChecker
    ]
}


/**
 * Exports
 */
export default ValidationMiddleware;
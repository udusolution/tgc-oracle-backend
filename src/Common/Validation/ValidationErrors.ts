import { ValidationError } from "express-validator";

export class CustomValidationError extends Error {
    validationErrors: Array<ValidationError>;
    constructor(errors: Array<ValidationError>) {
        super('Validation errors occurred');
        this.name = 'CustomValidationError';
        this.validationErrors = errors;
    }
}
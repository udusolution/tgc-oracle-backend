export class Validator {

    // Properties
    _value: any;
    _name: string;
    _isSet: boolean = false;
    _isValid: boolean = true;
    _isRequired: boolean = false;
    _errors: string[] = [];

    // Constructor
    constructor(val: any, name: string) {
        this._value = val;
        if(val) this._isSet = true;
        this._name = name;
    }

    // Methods
    required() {
        this._isRequired = true;
        if(!this._isSet) {
            this._isValid = false;
            this._errors.push(`${this._name} is required`);
        }
        return this;
    }
    
    number() {
        if(!this._isSet) return this;
        if(typeof this._value !== 'number') {
            this._isValid = false;
            this._errors.push(`${this._name} must be of type "number"`);
        }
        return this;
    }
    string() {
        if(!this._isSet) return this;
        if(typeof this._value !== 'string') {
            this._isValid = false;
            this._errors.push(`${this._name} must be of type "string"`);
        }
        return this;
    }
    boolean() {
        if(!this._isSet) return this;
        if(typeof this._value !== 'boolean') {
            this._errors.push(`${this._name} must be of type "boolean"`);
            this._isValid = false;
        }
        return this;
    }
    array() {
        if(!this._isSet) return this;
        if(!Array.isArray(this._value)) {
            this._isValid = false;
            this._errors.push(`${this._name} must be an array`);
        }
        return this;
    }

    throw() {
        if(this._errors.length > 0) throw new Error(this._errors.join('. '));
    }
    isValid(): boolean {
        return this._isValid;
    }
    isInvalid(): boolean {
        return !this._isValid;
    }
    
}
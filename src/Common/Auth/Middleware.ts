/**
 * @description middleware for user auth
 */

import { NextFunction } from "express";
import { Admin } from "../../Models/Admin";
import { User } from "../../Models/User";
import AdminRepo from "../../Repos/admin.repo";
import UserRepo from "../../Repos/user.repo";
import { Req, Res } from "../Interfaces/ExpressInterfaces";
import { IRouteLogger } from "../Interfaces/LoggerInterfaces";
import { JwtPayload } from "../Utils/JWT/Interfaces";
import JWT from "../Utils/JWT/JWT";
import { ForbiddenError, MissingJwtSecretAuthError, UnauthorizedError } from "./Errors";
import { IAuthRules, IUser, Role } from "./Interfaces";


/**
 * authentication middleware for user-related endpoints
 * @param privileges array of privileges that a user must have one of to use the endpoint
 * @returns auth middleware
 */
function UserAuth(privileges?: string[]) {
    return async function(req: Req<any>, res: Res<any>, next: NextFunction) {

        const log: IRouteLogger = req.log;
        const reqId: string = req.reqId;
        const fnName: string = "UserAuth";
        log.info(reqId, `${fnName} ### start`);

        try {

            let userId: number;

            // -> Get Token
            const token: string | undefined = req.header('token');
            const impersonate: string | undefined = req.header('impersonate');
            const userType: string | undefined = req.header('user-type');

            // ? If wrong user type, fail
            if(userType != 'user') throw new UnauthorizedError();

            // If the environment is development and the "impersonate" header is passed, then set the userId as the number in the impersonate header
            if(process.env.NODE_ENV === 'development' && impersonate && !isNaN(parseInt(impersonate))) {
                userId = parseInt(impersonate);
            }
            else {
                // Sanity Checks, throw error in the following cases
                // ! If no token
                // ! If no JWT secret set in ENV
                if(!token)                          throw new UnauthorizedError('authentication token missing from the header');
                if(!process.env.JWT_TOKEN_SECRET)   throw new MissingJwtSecretAuthError('JWT secret missing from ENV. Key must be JWT_TOKEN_SECRET');
    
                // -> Verify the Token
                const jwtPayload: JwtPayload = JWT.verify(token, process.env.JWT_TOKEN_SECRET);
                userId = jwtPayload.id;
            }

            // -> Get User
            const user: User | null = await UserRepo.findById(log, reqId, userId);

            // Sanity Checks, throw error in the following cases
            // ? If user not found
            // ? If user is banned
            // ? If user does not have required privilege
            if(!user)                                                                                               throw new UnauthorizedError('user belonging to this token was not found');
            if(user.is_banned)                                                                                      throw new UnauthorizedError('this user has been banned');
            if(privileges && privileges.length > 0 && (!user.privilege || !privileges?.includes(user.privilege)))   throw new ForbiddenError('user does not have required privilege');

            // <> Add user into req
            req.user = user;

            // End
            next();

        }
        catch(e: any) {
            next(e);
        }

    }
}

/**
 * authentication middleware for admin-related endpoints
 * @param roles array of roles that an admin must have one of to use the endpoint
 * @returns auth middleware
 */
function AdminAuth(roles?: Role[]) {
    return async function(req: Req<any>, res: Res<any>, next: NextFunction) {

        const log: IRouteLogger = req.log;
        const reqId: string = req.reqId;
        const fnName: string = "AdminAuth";
        log.info(reqId, `${fnName} ### start`);

        try {

            let userId: number;

            // -> Get Token
            const token: string | undefined = req.header('token');
            const impersonate: string | undefined = req.header('impersonate');
            const userType: string | undefined = req.header('user-type');
            log.info(reqId, `${fnName} ### header ### token: "${token}", user-type: "${userType}", impersonate: ${impersonate}`);

            // ? If wrong user type, fail
            if(userType != 'admin') {
                log.info(reqId, `${fnName} ## end [failure] ### incorrect user-type header. Expected "admin" but got "${userType}"`);
                throw new UnauthorizedError();
            }

            // If the environment is development and the "impersonate" header is passed, then set the userId as the number in the impersonate header
            if(process.env.NODE_ENV === 'development' && impersonate && !isNaN(parseInt(impersonate))) {
                userId = parseInt(impersonate);
                log.info(reqId, `${fnName} ## [dev] impersonating user with id ${userId}`);
            }
            else {
                // Sanity Checks, throw error in the following cases
                // ! If no token
                // ! If no JWT secret set in ENV
                if(!token)                          throw new UnauthorizedError('authentication token missing from the header');
                if(!process.env.JWT_TOKEN_SECRET)   throw new MissingJwtSecretAuthError('JWT secret missing from ENV. Key must be JWT_TOKEN_SECRET');
    
                // -> Verify the Token
                const jwtPayload: JwtPayload = JWT.verify(token, process.env.JWT_TOKEN_SECRET);
                userId = jwtPayload.id;
            }

            log.info(reqId, `${fnName} ## calling (AdminRepo)'findById'`);
            // -> Get Admin
            const admin: Admin | null = await AdminRepo.findById(log, reqId, userId);

            // Sanity Checks, throw error in the following cases
            // ? If admin not found
            // ? If admin does not have required role
            if(!admin)                                                       throw new UnauthorizedError('admin belonging to this token was not found');
            if(roles && roles.length > 0 && !roles?.includes(admin.role))    throw new ForbiddenError('admin does not have required role(s)');

            // <> Add admin into req
            req.admin = admin;

            // End
            next();

        }
        catch(e: any) {
            next(e);
        }

    }
}

/**
 * authentication middleware for endpoints shared by admins and users
 * @returns auth middleware
 */
function HybridAuth() {
    return async function(req: Req<any>, res: Res<any>, next: NextFunction) {

        const log: IRouteLogger = req.log;
        const reqId: string = req.reqId;
        const fnName: string = "HybridAuth";
        log.info(reqId, `${fnName} ### start`);

        try {

            let userId: number;

            // -> Get Token
            const token: string | undefined = req.header('token');
            const impersonate: string | undefined = req.header('impersonate');
            const userType: string | undefined = req.header('user-type');

            // ? If no user type, fail
            if(userType != 'user' && userType != 'admin') throw new UnauthorizedError();

            // If the environment is development and the "impersonate" header is passed, then set the userId as the number in the impersonate header
            if(process.env.NODE_ENV === 'development' && impersonate && !isNaN(parseInt(impersonate))) {
                userId = parseInt(impersonate);
            }
            else {
                // Sanity Checks, throw error in the following cases
                // ! If no token
                // ! If no JWT secret set in ENV
                if(!token)                          throw new UnauthorizedError('authentication token missing from the header');
                if(!process.env.JWT_TOKEN_SECRET)   throw new MissingJwtSecretAuthError('JWT secret missing from ENV. Key must be JWT_TOKEN_SECRET');
    
                // -> Verify the Token
                const jwtPayload: JwtPayload = JWT.verify(token, process.env.JWT_TOKEN_SECRET);
                userId = jwtPayload.id;
            }

            // If user is admin, get his data
            if(userType === 'admin') {

                // -> Get Admin
                const admin: Admin | null = await AdminRepo.findById(log, reqId, userId);
    
                // Sanity Checks, throw error in the following cases
                // ? If admin not found
                if(!admin) throw new UnauthorizedError('admin belonging to this token was not found');

                // <> Add admin into req
                req.admin = admin;
            }
            // Else, treat as user and get his data
            else {

                // -> Get User
                const user: User | null = await UserRepo.findById(log, reqId, userId);

                // Sanity Checks, throw error in the following cases
                // ? If user not found
                // ? If user is banned
                if(!user)             throw new UnauthorizedError('user belonging to this token was not found');
                if(user.is_banned)    throw new UnauthorizedError('this user has been banned');

                // <> Add user into req
                req.user = user;
            }

            // End
            next();

        }
        catch(e: any) {
            next(e);
        }

    }
}

/**
 * Exports
 */
const Auth = {
    UserAuth,
    AdminAuth,
    HybridAuth
}
export default Auth;
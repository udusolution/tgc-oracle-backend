export interface IUser {
    id: number,
    username: string,
    email: string,
    avatar?: string,
    password?: string,
    privilege?: string,
    is_banned: boolean,
    is_deleted: boolean,
    createdOn: string | Date | moment.Moment,
    updatedOn: string | Date | moment.Moment,
}

export interface IAdmin {
    id: number,
    username: string,
    email: string,
    avatar?: string,
    role: Role,
    password?: string,
    is_deleted: boolean,
    createdOn: string | Date | moment.Moment,
    updatedOn: string | Date | moment.Moment,
}

export type Role = string;
export type IAuthRules = Array<Role>;
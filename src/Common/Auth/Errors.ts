export class AuthError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = "AuthError";
    }
}

export class UnauthorizedError extends AuthError {
    constructor(message?: string) {
        super(message);
        this.name = "UnauthorizedError";
    }
}

export class ForbiddenError extends AuthError {
    constructor(message?: string) {
        super(message);
        this.name = "ForbiddenError";
    }
}

export class MissingJwtSecretAuthError extends AuthError {
    constructor(message?: string) {
        super(message);
        this.name = "MissingJwtSecretAuthError";
    }
}

export class RefreshTokenError extends AuthError {
    constructor(message?: string) {
        super(message);
        this.name = "RefreshTokenError";
    }
}
import { Send, Request } from "express-serve-static-core";
import { StatusCodesType } from "../../Enums/StatusCodes";

// Failure Response
export interface XFailResponse {
    message: string,
    error?: any,
    errors?: any[],
    errorCode?: number | string
}

// Response json() parameter
export interface ResJson<T> {
    message: string,
    data: T
}

// Response Express Object
export interface Res<T> extends Express.Response {   
    json: Send<ResJson<T> | XFailResponse, Res<T>>,
    status(code: StatusCodesType): Res<T>
}

// Request Express Object
export interface Req<T> extends Request {
    body: T
}
// Allow any extra properties in an interface
export interface IAny {
    [x: string]: unknown
}
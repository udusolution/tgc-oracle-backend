// Route Logger
export interface IRouteLogger {
    log(reqId: string | undefined, level: string, message: string): void;
    error(reqId: string | undefined, message: string): void;
    warn(reqId: string | undefined, message: string): void;
    verbose(reqId: string | undefined, message: string): void;
    info(reqId: string | undefined, message: string): void;
    debug(reqId: string | undefined, message: string): void;
    silly(reqId: string | undefined, message: string): void;
}
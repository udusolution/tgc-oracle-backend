import moment from "moment";

/**
 * Data Interfaces
 */
interface IDataTimestamps {
    createdOn: string | Date | moment.Moment,
    updatedOn: string | Date | moment.Moment,
}

export interface IService extends IDataTimestamps {
    id: number,
    service_category_id: number,
    title: string,
    description?: string,
    mode: string,
    provider?: string,
    rate_per_1000?: number,
    min_order?: number,
    max_order?: number,
    increment?: number,
    overflow?: number,
    is_deleted: boolean,
    service_category?: IServiceCategory
}

export interface IServiceCategory extends IDataTimestamps {
    id: number,
    title: string,
    description?: string,
    is_deleted: boolean,
    services?: IService[]
}

export interface IServiceWithCategory extends IService {
    service_category?: IServiceCategory;
}
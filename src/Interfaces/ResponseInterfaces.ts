/**
 * Response Interfaces
 */

export interface XTokens {
    accessToken: string,
    refreshToken: string
}

export interface XAccessToken {
    accessToken: string
}
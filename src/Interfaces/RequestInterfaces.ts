/**
 * Request Interfaces
 */
export interface RId {
    id: number
}

export interface RLogin {
    username: string,
    password: string
}

export interface RAddUser {
    username: string,
    email: string,
    password: string
}

export interface RRefresh {
    refreshToken: string
}

export interface RAddAdmin {
    username: string,
    email: string,
    password: string,
    role: string
}
# Express Typescript Template

## Overview

A template for monolithic nodejs express REST API using typescript.

## Environment Variables

Add a .env in the src folder.
Keys that are supposed to be used in the environment variables should be defined in src/environment.d.ts to allow intellisense.
Services and Utils implemented use environment variables for their configurations. Those variables are specified in the src/environment.d.ts file and mentioned in each service/utility implementation file.

## Transpile

Source code should be written in the src folder.
Run "npm run-script build" for the JS code to be generated into the dist folder.
Note that the .env will not be copied into the dist folder automatically. You should include the .env file manually wherever you try to deploy the dist code to.

## Services Implemented

### Mailers

For sending emails. The module to use is in "src/Common/Services/Mail/Mail.ts"

#### MailGun

Used for sending emails. You must define a domain there, and include the required ENV variables.

### SMS Senders

For sending SMS. The module to use is in "src/Common/Services/SMS/SMS.ts"

#### Twilio

Used for sending SMS. You must define a service there, and include the required ENV variables.
